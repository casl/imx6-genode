/*
 * \brief  Frame-buffer driver for Freescale's i.MX53
 * \author Nikolay Golikov <nik@ksyslabs.org>
 * \date   2012-06-21
 */

/* Genode includes include gpio_session/connection.h removed */
#include <drivers/board_base.h>
#include <os/attached_io_mem_dataspace.h>
#include <io_mem_session/connection.h>

#include <platform_session/connection.h>
#include <util/mmio.h>

/* local includes */
#include <ipu.h>
#include <pwm.h>


namespace Framebuffer {
	using namespace Genode;
	class Driver;
};

typedef unsigned int reg32_t; 

typedef union _hw_iomuxc_sw_mux_ctl_pad_gpio09
{
    reg32_t U;
    struct _hw_iomuxc_sw_mux_ctl_pad_gpio09_bitfields
    {
        unsigned MUX_MODE : 3; //!< [2:0] MUX Mode Select Field.
        unsigned RESERVED0 : 1; //!< [3] Reserved
        unsigned SION : 1; //!< [4] Software Input On Field.
        unsigned RESERVED1 : 27; //!< [31:5] Reserved
    } B;
} hw_iomuxc_sw_mux_ctl_pad_gpio09_t;

#define REGS_IOMUXC_BASE 			  (0x020e0000)
#define HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09_ADDR      (REGS_IOMUXC_BASE + 0x228)
#define HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09           (*(volatile hw_iomuxc_sw_mux_ctl_pad_gpio09_t *) HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09_ADDR)
#define HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09_RD()      (HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09.U)
#define HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09_WR(v)     (HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09.U = (v))

typedef union _hw_gpio_dr
{
    reg32_t U;
    struct _hw_gpio_dr_bitfields
    {
        unsigned DR : 32; //!< [31:0] Data bits.
    } B;
} hw_gpio_dr_t;

#define HW_GPIO_ADDR 	       (0x0209c000)
#define HW_GPIO_DR             (*(volatile hw_gpio_dr_t *) HW_GPIO_ADDR)
#define HW_GPIO_DR_RD()        (HW_GPIO_DR.U)
#define HW_GPIO_DR_WR(v)       (HW_GPIO_DR.U = (v))

typedef union _hw_gpio_gdir
{
    reg32_t U;
    struct _hw_gpio_gdir_bitfields
    {
        unsigned GDIR : 32; //!< [31:0] GPIO direction bits.
    } B;
} hw_gpio_gdir_t;

#define HW_GPIO_GDIR_ADDR    	  (HW_GPIO_ADDR + 0x4)
#define HW_GPIO_GDIR              (*(volatile hw_gpio_gdir_t *) HW_GPIO_GDIR_ADDR)
#define HW_GPIO_GDIR_RD()         (HW_GPIO_GDIR.U)
#define HW_GPIO_GDIR_WR(v)        (HW_GPIO_GDIR.U = (v))



class Framebuffer::Driver
{
	private:

		Platform::Connection              _platform;
		Attached_io_mem_dataspace         _ipu_mmio;
		Ipu                               _ipu;
		Attached_io_mem_dataspace         _pwm_mmio;
		Pwm                               _pwm;
		Platform::Session::Board_revision _board;
		size_t                            _width;
		size_t                            _height;

	public:

		enum Resolutions {
			QSB_WIDTH        = 800,
			QSB_HEIGHT       = 480,
			SMD_WIDTH        = 1024,
			SMD_HEIGHT       = 768,
			BYTES_PER_PIXEL  = 2,
		};

		enum Gpio_pins {
			LCD_BL_GPIO      = 88,
			LCD_CONT_GPIO    = 1,
		};

		Driver()
		: _ipu_mmio(Board_base::IPU_1_BASE, Board_base::IPU_1_SIZE),
		  /*
		   *		  Using (addr_t)IPU_1_BASE directly instead of local_addr
		   *		  of _ipu_mmio to initialise/write to physical address.

		   *		  _ipu((addr_t)_ipu_mmio.local_addr<void>()),
		   */
		  _ipu((addr_t)_ipu_mmio.local_addr<void>()),
		  _pwm_mmio(Board_base::PWM_2_BASE, Board_base::PWM_2_SIZE),
		  _pwm((addr_t)_pwm_mmio.local_addr<void>()),
		  _board(_platform.revision()),
		  _width(_board == Platform::Session::QSB ? QSB_WIDTH : SMD_WIDTH),
		  _height(_board == Platform::Session::QSB ? QSB_HEIGHT : SMD_HEIGHT) { }

		bool init(addr_t phys_base)
		{
			/* enable IPU via platform driver */
			_platform.enable(Platform::Session::IPU);

			switch (_board) {
			case Platform::Session::QSB:
				{
				PDBG("driver init happening with QSB mode");
				_ipu.init(_width, _height, _width * BYTES_PER_PIXEL,
				          phys_base, true);
				reg32_t mux_val;
				reg32_t oldVal = 0, newVal = 0;

				mux_val = HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09_RD();
				HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09_WR(0x5);	// 0x5 corresponds to GPIO1_IO09 mode
	
				oldVal = HW_GPIO_GDIR_RD();
				newVal = oldVal | (1 << 9);
				HW_GPIO_GDIR_WR(newVal);	

				reg32_t value = HW_GPIO_DR_RD();   // read current value

				value |= (1 << 9);
	
				HW_GPIO_DR_WR(value);
				HW_IOMUXC_SW_MUX_CTL_PAD_GPIO09_WR(mux_val);
				/* turn display on */
				/*Gpio::Connection gpio_bl(LCD_BL_GPIO);
				gpio_bl.direction(Gpio::Session::OUT);
				gpio_bl.write(true);
				Gpio::Connection gpio_ct(LCD_CONT_GPIO);
				gpio_ct.direction(Gpio::Session::OUT);
				gpio_ct.write(true);*/
				break;
				}
			case Platform::Session::SMD:
				_ipu.init(_width, _height, _width * BYTES_PER_PIXEL,
				          phys_base, false);
				_platform.enable(Platform::Session::PWM);
				_pwm.enable_display();
				break;
			default:
				PERR("Unknown board revision!");
				return false;
			}
			return true;
		}

		Mode mode() { return Mode(_width, _height, Mode::RGB565); }
		Ipu &ipu()  { return _ipu; }
};
